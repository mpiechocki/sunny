@testable import Sunny
import Quick
import Nimble
import Foundation

class SavedLocationsProviderSpec: QuickSpec {

    override func spec() {
        var userDefaults: UserDefaultsProviderSpy!
        var encoder: JSONEncoder!
        var decoder: JSONDecoder!
        var sut: SavedLocationsProvider!

        beforeEach {
            userDefaults = UserDefaultsProviderSpy()
            encoder = JSONEncoder()
            decoder = JSONDecoder()
            sut = SavedLocationsProvider(userDefaultsProvider: userDefaults)
        }

        afterEach {
            sut = nil
            userDefaults = nil
            encoder = nil
            decoder = nil
        }

        describe("loading saved location") {
            var loadedLocations: [LocationInfo]?

            context("when there are no saved locations") {
                beforeEach {
                    userDefaults.stubbedObject = nil

                    loadedLocations = sut.loadLocations()
                }

                it("should call for user defaults object") {
                    expect(userDefaults.objectForKeyCalledWith).to(haveCount(1))
                    expect(userDefaults.objectForKeyCalledWith.first) == "kSavedLocations"
                }

                it("should return empty array") {
                    expect(loadedLocations) == []
                }
            }

            context("when there are some saved locations") {
                beforeEach {
                    let data = try! encoder.encode([LocationInfo.name("location 1"), LocationInfo.coordinates(.init(lat: 2, lon: 4))])
                    userDefaults.stubbedObject = data as Any?

                    loadedLocations = sut.loadLocations()
                }

                it("should call for user defaults object") {
                    expect(userDefaults.objectForKeyCalledWith).to(haveCount(1))
                    expect(userDefaults.objectForKeyCalledWith.first) == "kSavedLocations"
                }

                it("should return empty array") {
                    expect(loadedLocations) == [LocationInfo.name("location 1"), LocationInfo.coordinates(.init(lat: 2, lon: 4))]
                }
            }
        }

        describe("saving location") {
            context("when there are no location saved yet") {
                beforeEach {
                    userDefaults.stubbedObject = nil

                    sut.saveLocation(info: .name("location 2"))
                }

                it("should save array with one item") {
                    expect(userDefaults.setCalledWith).to(haveCount(1))
                    expect(userDefaults.setCalledWith.first?.key) == "kSavedLocations"
                    let saved = try! decoder.decode([LocationInfo].self, from: userDefaults.setCalledWith.first!.value as! Data)
                    expect(saved) == [.name("location 2")]
                }
            }

            context("when there ARE saved locations") {
                beforeEach {
                    let array = [
                        LocationInfo.name("location 3"),
                        LocationInfo.name("location 4"),
                        LocationInfo.coordinates(.init(lat: 1, lon: 14))
                    ]
                    let data = try! encoder.encode(array)
                    userDefaults.stubbedObject = data as Any?

                    sut.saveLocation(info: .coordinates(.init(lat: -8, lon: 2)))
                }

                it("should save array with all items") {
                    expect(userDefaults.setCalledWith).to(haveCount(1))
                    expect(userDefaults.setCalledWith.first?.key) == "kSavedLocations"
                    let saved = try! decoder.decode([LocationInfo].self, from: userDefaults.setCalledWith.first!.value as! Data)
                    expect(saved) == [
                        .name("location 3"),
                        .name("location 4"),
                        .coordinates(.init(lat: 1, lon: 14)),
                        .coordinates(.init(lat: -8, lon: 2))
                    ]
                }
            }
        }
    }

}
