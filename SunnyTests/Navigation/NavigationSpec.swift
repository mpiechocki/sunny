@testable import Sunny
import Quick
import Nimble
import UIKit

class NavigationSpec: QuickSpec {

    override func spec() {
        var navigationController: NavigationControllerSpy!
        var rootController: UIViewController!
        var createCalledForRoute: [Route]!
        var stubbedController: UIViewController!
        var sut: Navigation!

        beforeEach {
            rootController = UIViewController()
            navigationController = NavigationControllerSpy()
            createCalledForRoute = []
            stubbedController = UIViewController()
            sut = Navigation(navigationController: navigationController) {
                createCalledForRoute.append($0)
                return stubbedController
            }
        }

        afterEach {
            sut = nil
            rootController = nil
            navigationController = nil
            createCalledForRoute = nil
            stubbedController = nil
        }

        context("when going to route") {
            beforeEach {
                navigationController.stack = [rootController]

                sut.go(to: .location(.name("")))
            }

            it("should call for controller creation") {
                expect(createCalledForRoute).to(haveCount(1))
                expect(createCalledForRoute.first) == .location(.name(""))
            }

            it("should push created controller onto the stack") {
                expect(navigationController.viewControllers).to(haveCount(2))
                expect(navigationController.viewControllers.first) === rootController
                expect(navigationController.viewControllers.last) === stubbedController
                expect(navigationController.pushCalledWith).to(haveCount(1))
                expect(navigationController.pushCalledWith.first?.animated) == true
            }
        }
    }

}
