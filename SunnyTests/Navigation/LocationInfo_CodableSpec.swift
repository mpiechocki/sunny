@testable import Sunny
import Quick
import Nimble
import Foundation

class LocationInfo_CodableSpec: QuickSpec {

    override func spec() {
        describe("LocationInfo+Codable") {
            var encoder: JSONEncoder!
            var decoder: JSONDecoder!

            beforeEach {
                encoder = JSONEncoder()
                decoder = JSONDecoder()
            }

            afterEach {
                encoder = nil
                decoder = nil
            }

            context("encoding LocationInfo.name") {
                var data: Data!
                var encodedJson: String!

                beforeEach {
                    data = try! encoder.encode(LocationInfo.name("Some name"))
                    encodedJson = String(data: data, encoding: .utf8)
                }

                afterEach {
                    encodedJson = nil
                    data = nil
                }

                it("should encode correctly") {
                    expect(encodedJson) == "{\"name\":\"Some name\"}"
                }

                it("decode correctly") {
                    expect { try? decoder.decode(LocationInfo.self, from: data) } == .name("Some name")
                }
            }

            context("encoding LocationInfo.coordinates") {
                var data: Data!
                var encodedJson: String!

                beforeEach {
                    data = try! encoder.encode(LocationInfo.coordinates(.init(lat: 12.34, lon: -2.345)))
                    encodedJson = String(data: data, encoding: .utf8)
                }

                afterEach {
                    encodedJson = nil
                    data = nil
                }

                it("should encode correctly") {
                    expect(encodedJson) == "{\"coordinates\":{\"lat\":12.34,\"lon\":-2.3450000000000002}}"
                }

                it("decode correctly") {
                    expect { try? decoder.decode(LocationInfo.self, from: data) } == .coordinates(.init(lat: 12.34, lon: -2.3450000000000002))
                }
            }

            context("encoding an array") {
                var data: Data!
                var encodedJson: String!

                beforeEach {
                    data = try! encoder.encode([LocationInfo.name("Some name"), LocationInfo.coordinates(.init(lat: 2, lon: 4))])
                    encodedJson = String(data: data, encoding: .utf8)
                }

                afterEach {
                    encodedJson = nil
                    data = nil
                }

                it("should encode correctly") {
                    expect(encodedJson) == "[{\"name\":\"Some name\"},{\"coordinates\":{\"lat\":2,\"lon\":4}}]"
                }

                it("decode correctly") {
                    expect { try? decoder.decode([LocationInfo].self, from: data) } == [.name("Some name"), .coordinates(.init(lat: 2, lon: 4))]
                }
            }
        }
    }

}
