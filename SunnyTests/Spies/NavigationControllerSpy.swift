import UIKit

class NavigationControllerSpy: UINavigationController {

    private(set) var pushCalledWith: [(controller: UIViewController, animated: Bool)] = []

    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        pushCalledWith.append((viewController, animated))

        stack += [viewController]
    }

    var stack: [UIViewController] = []

    override var viewControllers: [UIViewController] {
        get { stack }
        set { stack = newValue }
    }

}
