@testable import Sunny

class UserDefaultsProviderSpy: UserDefaultsProviding {

    private(set) var objectForKeyCalledWith: [String] = []
    private(set) var setCalledWith: [(value: Any?, key: String)] = []
    var stubbedObject: Any?

    // MARK: - UserDefaultsProviding

    func object(forKey key: String) -> Any? {
        objectForKeyCalledWith.append(key)
        return stubbedObject
    }

    func set(_ value: Any?, forKey key: String) {
        setCalledWith.append((value, key))
    }

}
