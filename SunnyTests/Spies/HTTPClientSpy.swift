@testable import Sunny

class HTTPClientSpy: HTTPClientProtocol {

    var performRequestCallsCount = 0

    func performRequest<Resource>(for endpoint: GetEndpoint<Resource>,
                                  completion: @escaping (Result<Resource,HTTPClientError>) -> Void) where Resource: Decodable {
        performRequestCallsCount += 1
    }

}
