@testable import Sunny
import UIKit

class NavigationSpy: Navigating {

    var stubbedNavigationController = NavigationControllerSpy()
    var stubbedController = UIViewController()
    private(set) var goToCalledWith: [Route] = []

    // MARK: - Navigating

    var navigationController: UINavigationController { stubbedNavigationController }
    lazy var createController: (Route) -> UIViewController = { _ in self.stubbedController }

    func go(to route: Route) {
        goToCalledWith.append(route)
    }

}
