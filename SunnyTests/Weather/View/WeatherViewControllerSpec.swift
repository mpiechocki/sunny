@testable import Sunny
import Quick
import Nimble
import Foundation

class WeatherViewControllerSpec: QuickSpec {

    override func spec() {
        describe("WeatherViewController") {
            var userDefaults: UserDefaultsProviderSpy!

            beforeEach {
                userDefaults = UserDefaultsProviderSpy()
            }

            afterEach {
                userDefaults = nil
            }

            context("when initializing with .name") {
                var sut: WeatherViewController!

                beforeEach {
                    sut = WeatherViewController(
                        locationInfo: .name("Some location"),
                        dataSource: UpdatableArrayDataSource<WeatherRowItem, WeatherServiceError>(
                            cellIdentifier: "WeatherViewCell",
                            elements: [],
                            dataProvider: DataProviderStub(),
                            configureCellBlock: { _, _ in }
                        ),
                        savedLocationsProvider: SavedLocationsProvider(
                            userDefaultsProvider: userDefaults
                        )
                    )
                }

                afterEach {
                    sut = nil
                }

                context("when view is loaded") {
                    beforeEach {
                        sut.loadViewIfNeeded()
                    }

                    it("should have right bar button item") {
                        expect(sut.navigationItem.rightBarButtonItem).notTo(beNil())
                    }

                    it("should have correct title") {
                        expect(sut.title) == "Some location"
                    }

                    context("when tapping on right bar button item") {
                        beforeEach {
                            sut.perform(sut.navigationItem.rightBarButtonItem?.action)
                        }

                        it("should save correct item to saved locations") {
                            expect(userDefaults.setCalledWith).to(haveCount(1))
                            let saved = try! JSONDecoder().decode([LocationInfo].self, from: userDefaults.setCalledWith.first!.value as! Data)
                            expect(saved) == [.name("Some location")]
                        }
                    }
                }
            }

            context("when initializing with .current location") {
                var sut: WeatherViewController!

                beforeEach {
                    sut = WeatherViewController(
                        locationInfo: .currentLocation,
                        dataSource: UpdatableArrayDataSource<WeatherRowItem, WeatherServiceError>(
                            cellIdentifier: "WeatherViewCell",
                            elements: [],
                            dataProvider: DataProviderStub(),
                            configureCellBlock: { _, _ in }
                        ),
                        savedLocationsProvider: SavedLocationsProvider(
                            userDefaultsProvider: userDefaults
                        )
                    )
                }

                afterEach {
                    sut = nil
                }

                context("when view is loaded") {
                    beforeEach {
                        sut.loadViewIfNeeded()
                    }

                    it("should NOT have right bar button item") {
                        expect(sut.navigationItem.rightBarButtonItem).to(beNil())
                    }

                    it("should have correct title") {
                        expect(sut.title) == "Current Location"
                    }
                }
            }

            context("when initializing with .coordinates") {
                var sut: WeatherViewController!

                beforeEach {
                    sut = WeatherViewController(
                        locationInfo: .coordinates(.init(lat: 12.345, lon: -3.426)),
                        dataSource: UpdatableArrayDataSource<WeatherRowItem, WeatherServiceError>(
                            cellIdentifier: "WeatherViewCell",
                            elements: [],
                            dataProvider: DataProviderStub(),
                            configureCellBlock: { _, _ in }
                        ),
                        savedLocationsProvider: SavedLocationsProvider(
                            userDefaultsProvider: userDefaults
                        )
                    )
                }

                afterEach {
                    sut = nil
                }

                context("when view is loaded") {
                    beforeEach {
                        sut.loadViewIfNeeded()
                    }

                    it("should have right bar button item") {
                        expect(sut.navigationItem.rightBarButtonItem).notTo(beNil())
                    }

                    it("should have correct title") {
                        expect(sut.title) == "12.345,-3.426"
                    }

                    context("when tapping on right bar button item") {
                        beforeEach {
                            sut.perform(sut.navigationItem.rightBarButtonItem?.action)
                        }

                        it("should save correct item to saved locations") {
                            expect(userDefaults.setCalledWith).to(haveCount(1))
                            let saved = try! JSONDecoder().decode([LocationInfo].self, from: userDefaults.setCalledWith.first!.value as! Data)
                            expect(saved) == [.coordinates(.init(lat: 12.345, lon: -3.426))]
                        }
                    }
                }
            }
        }
    }

}

class DataProviderStub: ArrayDataProvider {
    func registerDataObserver(_ observer: @escaping WeatherRowItemsObserver) {}
    func refreshData() {}
}
