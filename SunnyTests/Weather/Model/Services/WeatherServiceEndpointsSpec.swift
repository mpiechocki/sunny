@testable import Sunny
import Quick
import Nimble
import CoreLocation

class WeatherServiceEndpointsSpec: QuickSpec {

    override func spec() {
        describe("WeatherServiceEndpoints") {
            describe("endpoint for weather in location") {
                var endpoint: GetEndpoint<WeatherResult>!

                beforeEach {
                    endpoint = WeatherServiceEndpoints.getWeather(for: CLLocation(latitude: 2.34, longitude: -0.342))
                }

                it("should return endpoint with correct parameters") {
                    expect(endpoint.path) == "http://api.openweathermap.org/data/2.5/weather"
                    expect(endpoint.parameters).to(haveCount(3))
                    expect(endpoint.parameters["lat"] as? Double) == 2.34
                    expect(endpoint.parameters["lon"] as? Double) == -0.342
                    expect(endpoint.parameters["APPID"] as? String) == "4e41c354247b9bff4a9fa26f51307ec7"
                }
            }

            describe("endpoint for weather in location") {
                var endpoint: GetEndpoint<WeatherResult>!

                beforeEach {
                    endpoint = WeatherServiceEndpoints.getWeather(for: "LocationName")
                }

                it("should return endpoint with correct parameters") {
                    expect(endpoint.path) == "http://api.openweathermap.org/data/2.5/weather"
                    expect(endpoint.parameters).to(haveCount(2))
                    expect(endpoint.parameters["q"] as? String) == "LocationName"
                    expect(endpoint.parameters["APPID"] as? String) == "4e41c354247b9bff4a9fa26f51307ec7"
                }
            }
        }
    }

}
