@testable import Sunny
import Quick
import Nimble

class NameWeatherServiceSpec: QuickSpec {

    override func spec() {
        var httpClient: HTTPClientSpy!
        var sut: NameWeatherService!

        beforeEach {
            httpClient = HTTPClientSpy()
            sut = NameWeatherService(name: "Some name", httpClient: httpClient)
        }

        afterEach {
            sut = nil
            httpClient = nil
        }

        context("when refreshing data") {
            beforeEach {
                sut.refreshData()
            }

            it("should call request on httpClient") {
                expect(httpClient.performRequestCallsCount) == 1
            }
        }

        context("when registering data observer") {
            var caughtResult: Result<[WeatherRowItem], WeatherServiceError>?

            beforeEach {
                sut.registerDataObserver {
                    caughtResult = $0
                }
            }

            context("when observer is called") {
                beforeEach {
                    sut.observer?(.success([]))
                }

                it("should emit correct result") {
                    expect(self.checkIfCorrectResult(caughtResult)) == true
                }
            }
        }
    }

    private func checkIfCorrectResult(_ caughtResult: Result<[WeatherRowItem], WeatherServiceError>?) -> Bool {
        if let caughtResult = caughtResult, case let .success(values) = caughtResult {
            return values.isEmpty
        }

        return false
    }

}
