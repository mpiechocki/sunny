@testable import Sunny
import Quick
import Nimble

class AppContextSpec: QuickSpec {

    override func spec() {
        var sut: AppContext!

        beforeEach {
            sut = AppContext()
        }

        afterEach {
            sut = nil
        }

        it("should contain navigation") {
            expect { _ = sut.navigation }.notTo(throwAssertion())
        }

        describe("creating controllers") {
            it("should create correct WeatherViewController for .location(.name) route") {
                expect(sut.navigation.createController(.location(.name("")))).to(beAnInstanceOf(WeatherViewController.self))
            }

            it("should create correct WeatherViewController for .location(.currentLocation) route") {
                expect(sut.navigation.createController(.location(.currentLocation))).to(beAnInstanceOf(WeatherViewController.self))
            }

            it("should create correct WeatherViewController for .location(.coordinates) route") {
                expect(sut.navigation.createController(.location(.coordinates(Coordinates(lat: 0, lon: 0))))).to(beAnInstanceOf(WeatherViewController.self))
            }

            it("should create WelcomeController for .welcome route") {
                expect(sut.navigation.createController(.welcome)).to(beAnInstanceOf(WelcomeController.self))
            }

            it("should create SavedLocationsController for .saved route") {
                expect(sut.navigation.createController(.saved)).to(beAnInstanceOf(SavedLocationsController.self))
            }
        }
    }

}
