@testable import Sunny
import Quick
import Nimble
import SnapshotTesting

class WelcomeControllerSpec: QuickSpec {

    override func spec() {
        describe("WelcomeController") {
            var navigation: NavigationSpy!
            var sut: WelcomeController!

            beforeEach {
                navigation = NavigationSpy()
                sut = WelcomeController(navigation: navigation)
            }

            afterEach {
                sut = nil
                navigation = nil
            }

            context("snapshot") {
                it("should have correct snapshot") {
                    assertSnapshot(matching: sut, as: .image(on: .iPhoneX), named: "initial")
                }
            }

            context("when view is loaded") {
                beforeEach {
                    sut.loadViewIfNeeded()
                }

                it("should have right bar button item") {
                    expect(sut.navigationItem.rightBarButtonItem).notTo(beNil())
                }

                context("when righ bar button is tapped") {
                    beforeEach {
                        sut.perform(sut.navigationItem.rightBarButtonItem?.action)
                    }

                    it("should navigate to saved locations route") {
                        expect(navigation.goToCalledWith).to(haveCount(1))
                        expect(navigation.goToCalledWith.first) == .saved
                    }
                }

                context("when use current location button is tapped") {
                    beforeEach {
                        sut.welcomeView.useCurrentLocationButton.sendActions(for: .touchUpInside)
                    }

                    it("should navigate to .location route") {
                        expect(navigation.goToCalledWith).to(haveCount(1))
                        expect(navigation.goToCalledWith.first) == .location(.currentLocation)
                    }
                }

                context("when check button is tapped") {
                    beforeEach {
                        sut.welcomeView.checkButton.sendActions(for: .touchUpInside)
                    }

                    it("should NOT call navigation") {
                        expect(navigation.goToCalledWith).to(haveCount(0))
                    }
                }

                context("when location name is entered") {
                    beforeEach {
                        sut.welcomeView.nameTextField.text = "LocationName"
                        sut.welcomeView.nameTextField.sendActions(for: .editingDidEnd)
                    }

                    context("when check button is tapped") {
                        beforeEach {
                            sut.welcomeView.checkButton.sendActions(for: .touchUpInside)
                        }

                        it("should navigate to .location(.name) route") {
                            expect(navigation.goToCalledWith).to(haveCount(1))
                            expect(navigation.goToCalledWith.first) == .location(.name("LocationName"))
                        }
                    }
                }

                context("when empty location name is entered") {
                    beforeEach {
                        sut.welcomeView.nameTextField.text = ""
                        sut.welcomeView.nameTextField.sendActions(for: .editingDidEnd)
                    }

                    context("when check button is tapped") {
                        beforeEach {
                            sut.welcomeView.checkButton.sendActions(for: .touchUpInside)
                        }

                        it("should NOT call navigation") {
                            expect(navigation.goToCalledWith).to(haveCount(0))
                        }
                    }
                }

                context("when correct location coordinates are entered") {
                    beforeEach {
                        sut.welcomeView.latitudeTextField.text = "12.342"
                        sut.welcomeView.latitudeTextField.sendActions(for: .editingDidEnd)
                        sut.welcomeView.longitudeTextField.text = "-1.343"
                        sut.welcomeView.longitudeTextField.sendActions(for: .editingDidEnd)
                    }

                    context("when check button is tapped") {
                        beforeEach {
                            sut.welcomeView.checkButton.sendActions(for: .touchUpInside)
                        }

                        it("should navigate to .location(.coordinates) route") {
                            expect(navigation.goToCalledWith).to(haveCount(1))
                            expect(navigation.goToCalledWith.first) == .location(.coordinates(.init(lat: 12.342, lon: -1.343)))
                        }
                    }
                }

                context("when incorrect location coordinates are entered") {
                    beforeEach {
                        sut.welcomeView.latitudeTextField.text = "ads"
                        sut.welcomeView.latitudeTextField.sendActions(for: .editingDidEnd)
                        sut.welcomeView.longitudeTextField.text = "-1.343"
                        sut.welcomeView.longitudeTextField.sendActions(for: .editingDidEnd)
                    }

                    context("when check button is tapped") {
                        beforeEach {
                            sut.welcomeView.checkButton.sendActions(for: .touchUpInside)
                        }

                        it("should NOT call navigation") {
                            expect(navigation.goToCalledWith).to(haveCount(0))
                        }
                    }
                }
            }
        }
    }

}
