@testable import Sunny
import Quick
import Nimble

class SavedLocationCellSpec: QuickSpec {

    override func spec() {
        var sut: SavedLocationCell!

        beforeEach {
            sut = SavedLocationCell(style: .default, reuseIdentifier: "id")
            sut.label.text = "some location"
        }

        afterEach {
            sut = nil
        }

        context("when preparing for reuse") {
            beforeEach {
                sut.prepareForReuse()
            }

            it("should clear label's text") {
                expect(sut.label.text).to(beNil())
            }
        }
    }

}
