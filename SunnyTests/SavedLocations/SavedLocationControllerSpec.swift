@testable import Sunny
import Quick
import Nimble
import SnapshotTesting
import Foundation

class SavedLocationsControllerSpec: QuickSpec {

    override func spec() {
        var userDefaults: UserDefaultsProviderSpy!
        var navigation: NavigationSpy!
        var sut: SavedLocationsController!

        beforeEach {
            userDefaults = UserDefaultsProviderSpy()
            navigation = NavigationSpy()
            sut = SavedLocationsController(
                savedLocationsProvider: SavedLocationsProvider(
                    userDefaultsProvider: userDefaults
                ),
                navigation: navigation
            )

            let data = try! JSONEncoder().encode(
                [
                    LocationInfo.name("location 1"),
                    LocationInfo.coordinates(.init(lat: 2, lon: 4)),
                    LocationInfo.name("location 2"),
                    LocationInfo.coordinates(.init(lat: -18.352, lon: 7.23))
                ]
            )
            userDefaults.stubbedObject = data as Any?
        }

        afterEach {
            sut = nil
            userDefaults = nil
            navigation = nil
        }

        context("snapshot") {
            it("should have correct snapshot") {
                assertSnapshot(matching: sut, as: .image(on: .iPhoneX), named: "initial")
            }
        }

        context("when view is loaded") {
            beforeEach {
                sut.loadViewIfNeeded()
            }

            context("when selecting a row") {
                beforeEach {
                    sut.tableView.delegate?.tableView?(sut.tableView, didSelectRowAt: .init(row: 1, section: 0))
                }

                it("should navigate to .location route") {
                    expect(navigation.goToCalledWith).to(haveCount(1))
                    expect(navigation.goToCalledWith.first) == .location(.coordinates(.init(lat: 2, lon: 4)))
                }
            }
        }
    }

}
