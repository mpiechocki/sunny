import UIKit

class Navigation: Navigating {

    let createController: (Route) -> UIViewController
    let navigationController: UINavigationController

    init(navigationController: UINavigationController, createController: @escaping (Route) -> UIViewController) {
        self.navigationController = navigationController
        self.createController = createController
    }

    // MARK: - Navigating

    func go(to route: Route) {
        let controller = createController(route)
        navigationController.pushViewController(controller, animated: true)
    }

}
