enum Route: Equatable {
    case welcome
    case location(LocationInfo)
    case saved
}
