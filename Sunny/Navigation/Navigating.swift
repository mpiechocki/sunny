import UIKit

protocol Navigating {
    var navigationController: UINavigationController { get }
    var createController: (Route) -> UIViewController { get }
    func go(to: Route)
}
