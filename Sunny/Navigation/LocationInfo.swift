enum LocationInfo: Equatable {
    case name(String)
    case coordinates(Coordinates)
    case currentLocation
}

struct Coordinates: Equatable, Codable {

    let lat: Double
    let lon: Double

}

extension LocationInfo: Codable {

    private enum CodingKeys: String, CodingKey {
        case name
        case coordinates
    }

    enum DecodingError: Error {
        case decoding(String)
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? values.decode(String.self, forKey: .name) {
            self = .name(value)
            return
        }
        if let value = try? values.decode(Coordinates.self, forKey: .coordinates) {
            self = .coordinates(value)
            return
        }
        throw DecodingError.decoding("Something went wrong!")
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        switch self {
        case .name(let name):
            try container.encode(name, forKey: .name)
        case .coordinates(let coordinates):
            try container.encode(coordinates, forKey: .coordinates)
        case .currentLocation:
            fatalError("Something went wrong. Did you try to encode currentLocation? It's impossible!")
        }
    }

}
