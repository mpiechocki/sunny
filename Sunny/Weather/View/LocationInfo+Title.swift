import UIKit

extension LocationInfo {

    var title: String {
        switch self {
        case let .coordinates(coordinates):
            return String(coordinates.lat) + "," + String(coordinates.lon)
        case let .name(name):
            return name
        case .currentLocation:
            return "Current Location"
        }
    }

}
