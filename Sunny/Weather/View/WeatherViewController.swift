//
//  WeatherViewController.swift
//  Sunny
//
//  Created by Polidea on 12/10/2017.
//  Copyright © 2017 Polidea. All rights reserved.
//

import UIKit

final class WeatherViewController: UIViewController {

    let locationInfo: LocationInfo

    init(locationInfo: LocationInfo,
         dataSource: UpdatableArrayDataSource<WeatherRowItem, WeatherServiceError>,
         savedLocationsProvider: SavedLocationsProvider) {
        self.locationInfo = locationInfo
        self.dataSource = dataSource
        self.savedLocationsProvider = savedLocationsProvider
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        let tableView = UITableView(frame: .zero)
        let control = UIRefreshControl()
        self.refreshControl = control
        control.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        tableView.addSubview(control)
        view = tableView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        title = locationInfo.title
        dataSource.reload()

        if locationInfo != .currentLocation {
            navigationItem.setRightBarButton(
                UIBarButtonItem(
                    barButtonSystemItem: .save,
                    target: self,
                    action: #selector(rightBarButtonItemAction)
                ), animated: false
            )
        }
    }

    // MARK: - Private

    private let dataSource: UpdatableArrayDataSource<WeatherRowItem, WeatherServiceError>
    private let savedLocationsProvider: SavedLocationsProvider
    private var refreshControl: UIRefreshControl!

    private var tableView: UITableView {
        guard let tableView = view as? UITableView else { fatalError("Wrong view subclass") }
        return tableView
    }

    @objc private func handleRefresh() {
        dataSource.reload()
    }

    private func configureTableView() {
        tableView.register(WeatherViewCell.self, forCellReuseIdentifier: dataSource.cellReuseIdentifier)
        tableView.dataSource = dataSource
        tableView.delegate = self
        tableView.separatorStyle = .none
        dataSource.registerReloadFinishedCallback({ [weak self] error in
            if self?.refreshControl.isRefreshing == true {
                self?.refreshControl.endRefreshing()
            }
            if let error = error {
                print("Error while getting the data: \(error)")
            } else {
                self?.tableView.reloadData()
            }
        })
    }

    @objc private func rightBarButtonItemAction() {
        savedLocationsProvider.saveLocation(info: locationInfo)
    }
}

extension WeatherViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}
