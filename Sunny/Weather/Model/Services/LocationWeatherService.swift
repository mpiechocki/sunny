import CoreLocation

class LocationWeatherService: ArrayDataProvider {

    var observer: WeatherRowItemsObserver?

    init(lat: Double, lon: Double, httpClient: HTTPClientProtocol) {
        self.lat = lat
        self.lon = lon
        self.httpClient = httpClient
    }

    // MARK: - ArrayDataProvider

    func registerDataObserver(_ observer: @escaping WeatherRowItemsObserver) {
        self.observer = observer
    }

    func refreshData() {
        let location = CLLocation(latitude: lat, longitude: lon)
        let endpoint = WeatherServiceEndpoints.getWeather(for: location)

        httpClient.performRequest(for: endpoint, completion: { [weak self] result in
            let mapped = result.mapBoth( { $0.rowItems }, right: { WeatherServiceError.network($0) })
            self?.observer?(mapped)
        })
    }

    // MARK: - Private

    private let httpClient: HTTPClientProtocol
    private let lat: Double
    private let lon: Double

}
