import CoreLocation

class NameWeatherService: ArrayDataProvider {

    var observer: WeatherRowItemsObserver?

    init(name: String, httpClient: HTTPClientProtocol) {
        self.name = name
        self.httpClient = httpClient
    }

    // MARK: - ArrayDataProvider

    func registerDataObserver(_ observer: @escaping WeatherRowItemsObserver) {
        self.observer = observer
    }

    func refreshData() {
        let endpoint = WeatherServiceEndpoints.getWeather(for: name)

        httpClient.performRequest(for: endpoint, completion: { [weak self] result in
            let mapped = result.mapBoth( { $0.rowItems }, right: { WeatherServiceError.network($0) })
            self?.observer?(mapped)
        })
    }

    // MARK: - Private

    private let httpClient: HTTPClientProtocol
    private let name: String

}
