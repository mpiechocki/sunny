//
//  WeatherProperty.swift
//  Sunny
//
//  Created by Polidea on 12/10/2017.
//  Copyright © 2017 Polidea. All rights reserved.
//

import Foundation

struct WeatherProperty {
    let key: String
    let value: String
}
