//
//  AppDelegate.swift
//  Sunny
//
//  Created by Polidea on 12/10/2017.
//  Copyright © 2017 Polidea. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    private let appContext = AppContext()

    func application(
        _: UIApplication,
        didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?
      ) -> Bool {
        window = UIWindow()
        window?.rootViewController = appContext.navigation.navigationController
        appContext.navigation.go(to: .welcome)
        window?.makeKeyAndVisible()
        return true
    }
}
