//
//  AppContext.swift
//  Sunny
//
//  Created by Polidea on 12/10/2017.
//  Copyright © 2017 Polidea. All rights reserved.
//

import Foundation
import UIKit

final class AppContext {
    lazy var welcomeVC: UIViewController = WelcomeController(navigation: navigation)

    lazy var navigation: Navigating = Navigation(navigationController: UINavigationController()) { [unowned self] route in
        switch route {
        case let .location(info):
            return self.makeWeatherController(with: info)
        case .welcome:
            return self.welcomeVC
        case .saved:
            return SavedLocationsController(
                savedLocationsProvider: self.savedLocationsProvider,
                navigation: self.navigation
            )
        }
    }

    // MARK: - Private

    private lazy var client = HTTPClient()
    private lazy var locationService = LocationService()
    private lazy var savedLocationsProvider = SavedLocationsProvider(userDefaultsProvider: UserDefaults.standard)

    private func makeWeatherController(with info: LocationInfo) -> UIViewController {
        switch info {
        case let .coordinates(coordinates):
            return makeWeatherController(lat: coordinates.lat, lon: coordinates.lon, info: info)
        case let .name(name):
            return makeWeatherController(name: name, info: info)
        case .currentLocation:
            return makeWeatherControllerForCurrentLocation(with: info)
        }
    }

    private func makeWeatherControllerForCurrentLocation(with info: LocationInfo) -> UIViewController {
        let dataSource = UpdatableArrayDataSource<WeatherRowItem, WeatherServiceError>(
            cellIdentifier: WeatherViewCell.identifier,
            elements: [],
            dataProvider: WeatherService(
                locationService: locationService,
                httpClient: client
            ),
            configureCellBlock: { (cell, item) in
                guard let cell = cell as? WeatherViewCell else {
                    return
                }
                cell.update(with: item)
            }
        )

        return WeatherViewController(
            locationInfo: info,
            dataSource: dataSource,
            savedLocationsProvider: savedLocationsProvider
        )
    }

    private func makeWeatherController(lat: Double, lon: Double, info: LocationInfo) -> UIViewController {
        let dataSource = UpdatableArrayDataSource<WeatherRowItem, WeatherServiceError>(
            cellIdentifier: WeatherViewCell.identifier,
            elements: [],
            dataProvider: LocationWeatherService(
                lat: lat,
                lon: lon,
                httpClient: client
            ),
            configureCellBlock: { (cell, item) in
                guard let cell = cell as? WeatherViewCell else {
                    return
                }
                cell.update(with: item)
            }
        )

        return WeatherViewController(
            locationInfo: info,
            dataSource: dataSource,
            savedLocationsProvider: savedLocationsProvider
        )
    }

    private func makeWeatherController(name: String, info: LocationInfo) -> UIViewController {
        let dataSource = UpdatableArrayDataSource<WeatherRowItem, WeatherServiceError>(
            cellIdentifier: WeatherViewCell.identifier,
            elements: [],
            dataProvider: NameWeatherService(
                name: name,
                httpClient: client
            ),
            configureCellBlock: { (cell, item) in
                guard let cell = cell as? WeatherViewCell else {
                    return
                }
                cell.update(with: item)
            }
        )

        return WeatherViewController(
            locationInfo: info,
            dataSource: dataSource,
            savedLocationsProvider: savedLocationsProvider
        )
    }
}
