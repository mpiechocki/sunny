import UIKit

class SavedLocationsProvider {

    init(userDefaultsProvider: UserDefaultsProviding) {
        self.userDefaultsProvider = userDefaultsProvider
    }

    func saveLocation(info: LocationInfo) {
        let existing = loadLocations()
        let toSet = existing + [info]
        guard let data = try? encoder.encode(toSet) else { return }
        userDefaultsProvider.set(data, forKey: savedLocationsKey)
    }

    func loadLocations() -> [LocationInfo] {
        guard let data = userDefaultsProvider.object(forKey: savedLocationsKey) as? Data else { return [] }
        return (try? decoder.decode([LocationInfo].self, from: data)) ?? []
    }

    // MARK: - Private

    private let savedLocationsKey = "kSavedLocations"
    private let userDefaultsProvider: UserDefaultsProviding
    private lazy var encoder = JSONEncoder()
    private lazy var decoder = JSONDecoder()

}
