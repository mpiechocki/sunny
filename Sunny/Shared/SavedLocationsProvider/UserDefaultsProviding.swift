import UIKit

protocol UserDefaultsProviding {
    func object(forKey: String) -> Any?
    func set(_ value: Any?, forKey: String)
}

extension UserDefaults: UserDefaultsProviding {}
