import UIKit

class WelcomeController: UIViewController {

    init(navigation: Navigating) {
        self.navigation = navigation
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) { nil }

    // MARK: - View

    private(set) lazy var welcomeView = WelcomeView()

    override func loadView() {
        view = welcomeView
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        welcomeView.useCurrentLocationButton.addTarget(
            self,
            action: #selector(useCurrentLocationAction),
            for: .touchUpInside
        )

        welcomeView.checkButton.addTarget(
            self,
            action: #selector(checkButtonAction),
            for: .touchUpInside
        )

        navigationItem.setRightBarButton(
            UIBarButtonItem(
                barButtonSystemItem: .bookmarks,
                target: self,
                action: #selector(openSavedLocationsAction)
            ),
            animated: false
        )
    }

    // MARK: - Private

    private let navigation: Navigating

    @objc private func useCurrentLocationAction() {
        navigation.go(to: .location(.currentLocation))
    }

    @objc private func checkButtonAction() {
        if let name = welcomeView.nameTextField.text, !name.isEmpty {
            navigation.go(to: .location(.name(name)))
        } else if let latString = welcomeView.latitudeTextField.text,
            !latString.isEmpty,
            let lonString = welcomeView.longitudeTextField.text,
            !lonString.isEmpty,
            let lat = Double(latString),
            let lon = Double(lonString) {
            navigation.go(to: .location(.coordinates(Coordinates(lat: lat, lon: lon))))
        }
    }

    @objc private func openSavedLocationsAction() {
        navigation.go(to: .saved)
    }

}
