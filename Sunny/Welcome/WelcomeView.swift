import UIKit

class WelcomeView: UIView {

    init() {
        super.init(frame: .zero)
        setupViews()
        setupLayout()
        backgroundColor = .groupTableViewBackground
    }

    required init?(coder: NSCoder) { nil }

    let stackView = UIStackView()
    let byNameLabel = UILabel()
    let nameStackView = UIStackView()
    let nameLabel = UILabel()
    let nameTextField = UITextField()
    let byCoordinatesLabel = UILabel()
    let latitudeStackView = UIStackView()
    let latitudeLabel = UILabel()
    let latitudeTextField = UITextField()
    let longitudeStackView = UIStackView()
    let longitudeLabel = UILabel()
    let longitudeTextField = UITextField()
    let checkButton = UIButton(type: .system)
    let orLabel = UILabel()
    let useCurrentLocationButton = UIButton(type: .system)

    private func setupViews() {
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.spacing = 5.0

        nameStackView.distribution = .fill
        nameStackView.spacing = 5.0
        byNameLabel.text = "By Name"
        byNameLabel.font = UIFont.preferredFont(forTextStyle: .caption2)

        nameLabel.text = "Name:"
        nameTextField.backgroundColor = .white

        byCoordinatesLabel.text = "By Coordinates"
        byCoordinatesLabel.font = UIFont.preferredFont(forTextStyle: .caption2)

        latitudeStackView.spacing = 10.0
        latitudeLabel.text = "Latitude:"
        latitudeTextField.backgroundColor = .white

        longitudeStackView.spacing = 10.0
        longitudeLabel.text = "Longitude:"
        longitudeTextField.backgroundColor = .white

        checkButton.setTitle("Check", for: .normal)

        orLabel.text = "or"
        orLabel.textAlignment = .center
        orLabel.font = UIFont.preferredFont(forTextStyle: .caption2)

        useCurrentLocationButton.setTitle("Use current location", for: .normal)
    }

    private func setupLayout() {
        [nameLabel, nameTextField].forEach(nameStackView.addArrangedSubview)
        [latitudeLabel, latitudeTextField].forEach(latitudeStackView.addArrangedSubview)
        [longitudeLabel, longitudeTextField].forEach(longitudeStackView.addArrangedSubview)
        [byNameLabel, nameStackView, byCoordinatesLabel, latitudeStackView,
         longitudeStackView, checkButton, orLabel, useCurrentLocationButton].forEach(stackView.addArrangedSubview)
        addSubview(stackView)

        stackView.setCustomSpacing(30.0, after: nameStackView)
        stackView.setCustomSpacing(30.0, after: checkButton)

        nameLabel.setContentHuggingPriority(.required, for: .horizontal)
        latitudeLabel.setContentHuggingPriority(.required, for: .horizontal)
        longitudeLabel.setContentHuggingPriority(.required, for: .horizontal)

        stackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 8.0),
            stackView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -8.0),
            stackView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 15.0)
        ])
    }

}
