import UIKit

class SavedLocationCell: UITableViewCell {

    static let reuseId = "SavedLocationCell"

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
    }

    required init?(coder: NSCoder) { nil }

    override func prepareForReuse() {
        label.text = nil
    }

    let label = UILabel()

    // MARK: - Private

    private func setupLayout() {
        label.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(label)
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16.0),
            label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16.0),
            label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16.0),
            label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16.0)
        ])
    }

}
