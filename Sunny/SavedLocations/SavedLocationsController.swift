import UIKit

class SavedLocationsController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    init(savedLocationsProvider: SavedLocationsProvider, navigation: Navigating) {
        self.savedLocationsProvider = savedLocationsProvider
        self.navigation = navigation
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) { nil }

    // MARK: - View

    private(set) lazy var tableView = UITableView(frame: .zero, style: .plain)

    override func loadView() {
        view = tableView
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
        locations = savedLocationsProvider.loadLocations()
    }

    // MARK: - UITableViewDataSource

    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        locations?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: SavedLocationCell.reuseId) as? SavedLocationCell
        else { fatalError("Something went wrong!") }

        cell.label.text = locations?[indexPath.row].title

        return cell
    }

    // MARK: - UITableViewDelegate

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let location = locations?[indexPath.row] else { return }
        navigation.go(to: .location(location))
    }

    // MARK: - Private

    private var locations: [LocationInfo]?
    private let savedLocationsProvider: SavedLocationsProvider
    private let navigation: Navigating

    private func setupTableView() {
        tableView.register(SavedLocationCell.self, forCellReuseIdentifier: SavedLocationCell.reuseId)
        tableView.delegate = self
        tableView.dataSource = self
    }

}
